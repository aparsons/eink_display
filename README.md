# ESP32C3 E-Ink Project

<img src="docs/fruit.png"/>

This project utilizes an ESP32C3 development kit and an Inky Impression 5.7" 7-color e-ink display. The ultimate goal of this project was to be able to download any image off the internet and send it over a network to the ESP32C3 which would send the image over SPI to the e-ink display it was connected to. And to that end, this project was a success so please feel free to try it out!

## Setup

### Software

Follow the installation instructions outlined on the official Espressif website: https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/linux-macos-setup.html. **NOTE**: After cloning the respository, checkout version v4.3 before installing the tools in your system (after Step 2 but before Step 3).

```bash
cd esp-idf && git checkout --recurse-submodule v4.3
```

After all of the paths have been exported and the toolchains have been installed, clone this project and recursively setup the submodules. From within project folder:

```bash
git submodule update --init --recursive
```

To avoid hardcoding WIFI credentials into the project, two environment variables must be exported that contain the SSID and password of the network you want the ESP device to connect to: `WIFI_SSID` and `WIFI_PASS`. Without these variables exported, the project will fail to build. If the SSID or password is incorrect, the ESP will fail to connect to the network.

```bash
export WIFI_SSID="your network name"
export WIFI_PASS="your network password"
```

### Hardware

<img src="docs/inky_impression_pinout.png" width="500" height="500"/>

**NOTE**: This pinout is flipped on the y-axis (2 is on the left side and 1 is on the right side) because the Raspberry Pi plugs into the Inky display upside-down.

| Inky Side | ESP Side |
|-----------|----------|
| 5V        | 5V       |
| 3V3       | 3V3      |
| GND       | GND      |
| Busy      | GPIO1    |
| Reset     | GPIO10   |
| Data/Comm | GPIO0    |
| MOSI      | GPIO7    |
| MISO      | N/C      |
| SCLK      | GPIO5    |
| CS        | GPIO9    |


## Building/Flashing

Within `source_code/build`, there are 3 bash scripts:

- `build.sh`: builds the project (make sure you've exported the ESP-IDF environment)
- `flash.sh`: flashes the ESP device and then opens a serial console
- `monitor.sh`: opens a serial console

For `flash.sh` and `monitor.sh`, you need to provide the serial port the ESP has been linked to as a CLI argument. For example:

```bash
./flash.sh /dev/USB0
```

## Sample Client

`scripts/send_data.py` can used as a starting point for sending any image you download to your ESP device. Unfortunately I didn't bother setting up a virtual environment as the script is so simple but there are only a few libraries to install to run the script.

```bash
python3 send_data.py <path to image> <saturation> <ip addr>
```

A sample image of some fruit has been provided. The saturation level is a floating point value with values ranging from 0 to 1.0. The IP address of your device should be printed through the console at startup.

The blending and saturation palettes in the script have been taken directly from Pimoroni's Github repository. This script was only meant to get things rolling without needing to use their entire repo.

## GoogleTest and GoogleMock

This project is configured to work with GoogleTest and GoogleMock for unit/integration testing on a desktop machine.

### Installation

Clone the latest GoogleTest release which at the time of writing this is 1.10.0 and then generate the Makefile with CMake

```bash
git clone https://github.com/google/googletest.git -b release-1.10.0
cd googletest        # Main directory of the cloned repository.
mkdir build && cd build
cmake ..             # Generate native build scripts for GoogleTest.
```

Build and install the GoogleTest and GoogleMock libraries

```bash
make -j8
sudo make install    # Installed in /usr/local/ by default
```

### Building and Running Tests

Navigate to the build directory and then create another build directory if one doesn't exist

```bash
cd source_code/tests/build
mkdir build && cd build
```

Generate the Makefile using CMake and then build the test project

```bash
cmake ..
make -j8
```

Run the test executable

```bash
./esp32c3_test.out
```
