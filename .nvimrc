lua << EOF
local nvim_lsp = require('lspconfig')

nvim_lsp.clangd.setup {
  on_attach=require'lsp_signature'.on_attach,
  init_options = {
    compilationDatabasePath="source_code/build/build",
    fallbackFlags={"-std=c++17"},
  },
  cmd = {
    "clangd",
    "--background-index",
    "--header-insertion=never",
  },
  filetypes = {"c", "cpp", "objc", "objcpp"},
}

EOF
