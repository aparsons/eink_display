#include "esp32c3.h"

namespace rtos {

RtosESP32C3::RtosESP32C3() {
}

RtosESP32C3::~RtosESP32C3() {
}

task_t RtosESP32C3::TaskCreate(const char *name,
                               void (*function)(void *),
                               uint32_t stack_size_bytes,
                               void *params,
                               uint32_t priority) const {
  TaskHandle_t rtos_task;

  BaseType_t rc = xTaskCreate(function, name, stack_size_bytes, params, priority, &rtos_task);

  if (rc != pdPASS) {
    return NULL;
  } else {
    return rtos_task;
  }
}

void RtosESP32C3::TaskDelete(task_t task) const {
  vTaskDelete(task);
}

void RtosESP32C3::TaskDelayMs(uint32_t ms_to_delay) const {
  vTaskDelay(ms_to_delay / portTICK_PERIOD_MS);
}

queue_t RtosESP32C3::QueueCreate(uint32_t num_items, uint32_t item_size) const {
  QueueHandle_t rtos_queue;

  rtos_queue = xQueueCreate(num_items, item_size);

  return rtos_queue;
}

int32_t RtosESP32C3::QueueReceive(queue_t queue, void *p_buffer, uint32_t ms_to_wait) const {
  BaseType_t rc = xQueueReceive(static_cast<QueueHandle_t>(queue), p_buffer, static_cast<TickType_t>(ms_to_wait));

  return rc;
}

int32_t RtosESP32C3::QueueSend(queue_t queue, const void *p_buffer, uint32_t ms_to_wait) const {
  BaseType_t rc = xQueueSend(static_cast<QueueHandle_t>(queue), p_buffer, static_cast<TickType_t>(ms_to_wait));

  return rc;
}

int32_t RtosESP32C3::SemaphoreTake(semaphore_t sem, uint32_t ms_to_wait) const {
  BaseType_t rc = xSemaphoreTake(static_cast<SemaphoreHandle_t>(sem), static_cast<TickType_t>(ms_to_wait));

  return rc;
}

int32_t RtosESP32C3::SemaphoreGive(semaphore_t sem) const {
  BaseType_t rc = xSemaphoreGive(static_cast<SemaphoreHandle_t>(sem));

  return rc;
}

evt_group_t RtosESP32C3::EventGroupCreate() const {
  return xEventGroupCreate();
}

evt_bits_t RtosESP32C3::EventGroupWaitBits(const evt_group_t group,
                                           const evt_bits_t bits_to_wait_for,
                                           const int32_t clear_on_exit,
                                           const int32_t wait_for_all_bits,
                                           uint32_t ms_to_wait) const {
  return xEventGroupWaitBits(static_cast<EventGroupHandle_t>(group),
                             bits_to_wait_for,
                             clear_on_exit,
                             wait_for_all_bits,
                             ms_to_wait);
}

evt_bits_t RtosESP32C3::EventGroupSetBits(const evt_group_t group, const evt_bits_t bits_to_set) const {
  return xEventGroupSetBits(static_cast<EventGroupHandle_t>(group), bits_to_set);
}

evt_bits_t RtosESP32C3::EventGroupClearBits(const evt_group_t group, const evt_bits_t bits_to_clear) const {
  return xEventGroupClearBits(static_cast<EventGroupHandle_t>(group), bits_to_clear);
}

};  // namespace rtos
