#pragma once

#include "../interface.h"
#include <array>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/event_groups.h"

namespace rtos {

class RtosESP32C3 : public IRtos {
 public:
  RtosESP32C3();
  ~RtosESP32C3();

  task_t TaskCreate(const char *name,
                    void (*function)(void *),
                    uint32_t stack_size_bytes,
                    void *params,
                    uint32_t priority) const;
  void TaskDelete(task_t task) const;
  void TaskDelayMs(uint32_t ms_to_delay) const;
  queue_t QueueCreate(uint32_t num_items, uint32_t item_size) const;
  int32_t QueueReceive(queue_t queue, void *p_buffer, uint32_t ms_to_wait) const;
  int32_t QueueSend(queue_t queue, const void *p_buffer, uint32_t ms_to_wait) const;
  int32_t SemaphoreTake(semaphore_t sem, uint32_t ms_to_wait) const;
  int32_t SemaphoreGive(semaphore_t sem) const;
  evt_group_t EventGroupCreate() const;
  evt_bits_t EventGroupWaitBits(const evt_group_t group,
                                const evt_bits_t bits_to_wait_for,
                                const int32_t clear_on_exit,
                                const int32_t wait_for_all_bits,
                                uint32_t ms_to_wait) const;
  evt_bits_t EventGroupSetBits(const evt_group_t group, const evt_bits_t bits_to_set) const;
  evt_bits_t EventGroupClearBits(const evt_group_t group, const evt_bits_t bits_to_clear) const;

 private:
};

};  // namespace rtos
