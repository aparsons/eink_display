#pragma once

#include <stdint.h>

namespace rtos {

typedef void *task_t;
typedef void *queue_t;
typedef void *semaphore_t;
typedef void *evt_group_t;
typedef uint32_t evt_bits_t;

class IRtos {
 public:
  virtual ~IRtos() {
  }

  virtual task_t TaskCreate(const char *name,
                            void (*function)(void *),
                            uint32_t stack_size_bytes,
                            void *params,
                            uint32_t priority) const                                                    = 0;
  virtual void TaskDelete(task_t task) const                                                            = 0;
  virtual void TaskDelayMs(uint32_t ms_to_delay) const                                                  = 0;
  virtual queue_t QueueCreate(uint32_t num_items, uint32_t item_size) const                             = 0;
  virtual int32_t QueueReceive(queue_t queue, void *p_buffer, uint32_t ms_to_wait) const                = 0;
  virtual int32_t QueueSend(queue_t queue, const void *p_buffer, uint32_t ms_to_wait) const             = 0;
  virtual int32_t SemaphoreTake(semaphore_t sem, uint32_t ms_to_wait) const                             = 0;
  virtual int32_t SemaphoreGive(semaphore_t queue) const                                                = 0;
  virtual evt_group_t EventGroupCreate() const                                                          = 0;
  virtual evt_bits_t EventGroupWaitBits(const evt_group_t group,
                                        const evt_bits_t bits_to_wait_for,
                                        const int32_t clear_on_exit,
                                        const int32_t wait_for_all_bits,
                                        uint32_t ms_to_wait) const                                      = 0;
  virtual evt_bits_t EventGroupSetBits(const evt_group_t group, const evt_bits_t bits_to_set) const     = 0;
  virtual evt_bits_t EventGroupClearBits(const evt_group_t group, const evt_bits_t bits_to_clear) const = 0;
};

};  // namespace rtos
