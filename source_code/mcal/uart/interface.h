#pragma once

#include <stdint.h>

namespace uart {

enum class BaudRate {
  RATE_9600   = 9600,
  RATE_19200  = 19200,
  RATE_38400  = 38400,
  RATE_115200 = 115200,
};

enum class Parity {
  NONE,
  EVEN,
  ODD,
};

enum class FlowControl {
  DISABLED,
  ENABLED,
};

enum class StopBits {
  BITS_1,
  BITS_1_5,
  BITS_2,
};

enum class CharSize {
  SIZE_5_BITS,
  SIZE_6_BITS,
  SIZE_7_BITS,
  SIZE_8_BITS,
};

class IUart {
 public:
  virtual ~IUart() {
  }

  virtual int32_t Init(BaudRate baud, Parity parity, FlowControl flow_ctrl, StopBits stop, CharSize size) = 0;
  virtual int32_t Uninit()                                                                                = 0;
  virtual int32_t ReadBytes(char *const buffer)                                                           = 0;
  virtual int32_t WriteBytes(const char *const buffer, uint8_t num_bytes)                                 = 0;
  virtual uint32_t GetBufferSize() const                                                                  = 0;
};

};  // namespace uart
