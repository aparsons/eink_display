#pragma once

#include "../interface.h"
#include "driver/uart.h"
#include <array>

namespace uart {

constexpr uint8_t BUFFER_SIZE = 255;
constexpr uint8_t QUEUE_SIZE  = 10;

class UartESP32C3 : public IUart {
 public:
  UartESP32C3(uart_port_t uart_port, SemaphoreHandle_t uart_bin_sem);
  ~UartESP32C3();

  int32_t Init(BaudRate baud, Parity parity, FlowControl flow_ctrl, StopBits stop, CharSize size);
  int32_t Uninit();
  int32_t ReadBytes(char *const buffer);
  int32_t WriteBytes(const char *const buffer, uint8_t num_bytes);
  uint32_t GetBufferSize() const;

  void ReadBytesTask();

 private:
  uart_port_t port;
  std::array<uint8_t, BUFFER_SIZE> rx_buff = {};
  std::array<uint8_t, BUFFER_SIZE> tx_buff = {};
  uint8_t head                             = 0;
  uint8_t tail                             = 0;
  bool is_full                             = false;

  QueueHandle_t queue;
  TaskHandle_t get_bytes_task;
  SemaphoreHandle_t get_bytes_mutex;
  SemaphoreHandle_t uart_bin_sem;

  uint8_t GetSize() const;
};

};  // namespace uart
