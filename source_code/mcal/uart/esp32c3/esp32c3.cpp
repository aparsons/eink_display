#include "esp32c3.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"

namespace uart {

constexpr uint16_t GET_BYTES_TASK_SIZE = 2 * 1024;

const char *TAG = "UART";

void GetBytesFreeTask(void *params);

UartESP32C3::UartESP32C3(uart_port_t uart_port, SemaphoreHandle_t uart_bin_sem)
    : port(uart_port), uart_bin_sem(uart_bin_sem) {
}

UartESP32C3::~UartESP32C3() {
}

int32_t UartESP32C3::Init(BaudRate baud, Parity parity, FlowControl flow_ctrl, StopBits stop, CharSize size) {
  esp_err_t rc;

  rc = uart_driver_install(port, BUFFER_SIZE, BUFFER_SIZE, QUEUE_SIZE, &queue, 0);
  if (rc != ESP_OK) { return rc; }

  uart_config_t config;
  config.baud_rate           = static_cast<int>(baud);
  config.rx_flow_ctrl_thresh = 0;
  config.source_clk          = UART_SCLK_APB;

  switch (parity) {
    case Parity::NONE:
      config.parity = UART_PARITY_DISABLE;
      break;
    case Parity::EVEN:
      config.parity = UART_PARITY_EVEN;
      break;
    case Parity::ODD:
      config.parity = UART_PARITY_ODD;
      break;
  }

  switch (flow_ctrl) {
    case FlowControl::DISABLED:
      config.flow_ctrl = UART_HW_FLOWCTRL_DISABLE;
      break;
    case FlowControl::ENABLED:
      config.flow_ctrl = UART_HW_FLOWCTRL_CTS_RTS;
      break;
  }

  switch (stop) {
    case StopBits::BITS_1:
      config.stop_bits = UART_STOP_BITS_1;
      break;
    case StopBits::BITS_1_5:
      config.stop_bits = UART_STOP_BITS_1_5;
      break;
    case StopBits::BITS_2:
      config.stop_bits = UART_STOP_BITS_2;
      break;
  }

  switch (size) {
    case CharSize::SIZE_5_BITS:
      config.data_bits = UART_DATA_5_BITS;
      break;
    case CharSize::SIZE_6_BITS:
      config.data_bits = UART_DATA_6_BITS;
      break;
    case CharSize::SIZE_7_BITS:
      config.data_bits = UART_DATA_7_BITS;
      break;
    case CharSize::SIZE_8_BITS:
      config.data_bits = UART_DATA_8_BITS;
      break;
  }

  rc = uart_param_config(port, &config);
  if (rc != ESP_OK) { return rc; }

  get_bytes_mutex = xSemaphoreCreateMutex();
  rc              = xTaskCreate(GetBytesFreeTask, "get_bytes", GET_BYTES_TASK_SIZE, this, 2, &get_bytes_task);

  if (rc == pdPASS) { rc = 0; }

  return rc;
}

int32_t UartESP32C3::Uninit() {
  return ESP_OK;
}

int32_t UartESP32C3::ReadBytes(char *const buffer) {
  int32_t bytes_available;

  // read out all available bytes one byte at a time before returning number of bytes read out

  if (xSemaphoreTake(get_bytes_mutex, 10) == pdTRUE) {
    bytes_available = GetSize();
    is_full         = false;

    for (auto buffer_index = 0; buffer_index < bytes_available; buffer_index++) {
      buffer[buffer_index] = rx_buff[tail];
      tail                 = (tail + 1) % rx_buff.size();
    }

    xSemaphoreGive(get_bytes_mutex);
  } else {
    // TODO: better error here?
    bytes_available = -1;
  }

  return bytes_available;
}

int32_t UartESP32C3::WriteBytes(const char *const buffer, uint8_t num_bytes) {
  return ESP_OK;
}

uint32_t UartESP32C3::GetBufferSize() const {
  return BUFFER_SIZE;
}

void UartESP32C3::ReadBytesTask() {
  ESP_LOGI(TAG, "Starting ReadBytesTask");
  uart_event_t event;
  bool send_semaphore = false;

  do {
    if (xQueueReceive(queue, &event, portMAX_DELAY)) {
      switch (event.type) {
        case UART_DATA:
          if (xSemaphoreTake(get_bytes_mutex, 10) == pdTRUE) {
            // read number of bytes indicated from event, one byte at a time
            for (auto i = 0; i < event.size; i++) {
              // read byte into buffer
              uart_read_bytes(port, &rx_buff[head], 1, portMAX_DELAY);

              // echo back character
              printf("%c", rx_buff[head]);
              fflush(stdout);

              if ((rx_buff[head] == '\n') || (rx_buff[head] == '\r')) {
                send_semaphore = true;
                // add extra newline for better readability
                printf("\n");
                fflush(stdout);
              }

              if (is_full) { tail = (tail + 1) % rx_buff.size(); }

              head = (head + 1) % rx_buff.size();

              is_full = (head == tail);

              if (send_semaphore) {
                // stop processing the input and notify CLI of new command
                send_semaphore = false;
                xSemaphoreGive(uart_bin_sem);
                break;
              }
            }

            xSemaphoreGive(get_bytes_mutex);
          } else {
            // TODO: handle error here
          }
          break;
        case UART_BREAK:
        case UART_BUFFER_FULL:
        case UART_FIFO_OVF:
        case UART_PARITY_ERR:
        case UART_FRAME_ERR:
        case UART_DATA_BREAK:
        case UART_PATTERN_DET:
        case UART_EVENT_MAX:
          break;
      }
    }
  } while (1);
}

uint8_t UartESP32C3::GetSize() const {
  uint8_t size = rx_buff.size();

  if (!is_full) {
    if (head >= tail) {
      size = head - tail;
    } else {
      size = (rx_buff.size() + head) - tail;
    }
  }

  return size;
}

void GetBytesFreeTask(void *params) {
  static_cast<UartESP32C3 *>(params)->ReadBytesTask();
}

};  // namespace uart
