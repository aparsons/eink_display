#pragma once

#include "../interface.h"
#include <array>

namespace gpio {

class GpioESP32C3 : public IGpio {
 public:
  GpioESP32C3();
  ~GpioESP32C3();

  uint8_t Read(uint8_t pin) const;
  int32_t Write(uint8_t pin, bool value) const;
  int32_t SetPull(uint8_t pin, PULL pull_type) const;
  int32_t SetDirection(uint8_t pin, DIRECTION direction) const;
};

};  // namespace gpio
