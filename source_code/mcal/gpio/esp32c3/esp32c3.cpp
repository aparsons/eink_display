#include "esp32c3.h"
#include "driver/gpio.h"

namespace gpio {

GpioESP32C3::GpioESP32C3() {
}

GpioESP32C3::~GpioESP32C3() {
}

uint8_t GpioESP32C3::Read(uint8_t pin) const {
  return gpio_get_level(static_cast<gpio_num_t>(pin));
}

int32_t GpioESP32C3::Write(uint8_t pin, bool value) const {
  return static_cast<int32_t>(gpio_set_level(static_cast<gpio_num_t>(pin), value));
}

int32_t GpioESP32C3::SetPull(uint8_t pin, PULL pull_type) const {
  gpio_pull_mode_t pull_mode;
  int32_t rc            = 0;
  bool valid_conversion = false;

  switch (pull_type) {
    case PULL::DISABLED:
      pull_mode        = GPIO_FLOATING;
      valid_conversion = true;
      break;
    case PULL::PULLDOWN:
      pull_mode        = GPIO_PULLDOWN_ONLY;
      valid_conversion = true;
      break;
    case PULL::PULLUP:
      pull_mode        = GPIO_PULLUP_ONLY;
      valid_conversion = true;
      break;
  }

  if (valid_conversion) {
    esp_err_t err_code;
    rc = gpio_set_pull_mode(static_cast<gpio_num_t>(pin), pull_mode);
  } else {
    rc = -1;
  }

  return rc;
}

int32_t GpioESP32C3::SetDirection(uint8_t pin, DIRECTION direction) const {
  gpio_mode_t mode;
  int32_t rc            = 0;
  bool valid_conversion = false;

  switch (direction) {
    case DIRECTION::INPUT:
      mode             = GPIO_MODE_INPUT;
      valid_conversion = true;
      break;
    case DIRECTION::OUTPUT:
      mode             = GPIO_MODE_OUTPUT;
      valid_conversion = true;
      break;
  }

  if (valid_conversion) {
    esp_err_t err_code;
    rc = gpio_set_direction(static_cast<gpio_num_t>(pin), mode);
  } else {
    rc = -1;
  }

  return rc;
}

};  // namespace gpio
