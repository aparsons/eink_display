#pragma once

#include <stdint.h>

namespace gpio {

enum class PULL {
  DISABLED,
  PULLDOWN,
  PULLUP,
};

enum class DIRECTION {
  INPUT,
  OUTPUT,
};

class IGpio {
 public:
  virtual ~IGpio() {
  }

  virtual uint8_t Read(uint8_t pin) const                              = 0;
  virtual int32_t Write(uint8_t pin, bool value) const                 = 0;
  virtual int32_t SetPull(uint8_t pin, PULL pull_type) const           = 0;
  virtual int32_t SetDirection(uint8_t pin, DIRECTION direction) const = 0;
};

};  // namespace gpio
