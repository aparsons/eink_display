#pragma once

#include "../interface.h"
#include "esp_http_server.h"

namespace http {

class HttpESP32C3 : public IHttp {
 public:
  HttpESP32C3();
  ~HttpESP32C3();

  int32_t Init(uri_t uri);
  int32_t Uninit();
  handle_t Start();
  uint32_t GetContentLength(req_t req) const;
  int32_t ReceiveData(req_t req, char *buf, uint32_t buf_size);
  int32_t SendData(req_t req, const char *buf, uint32_t buf_size);

 private:
  httpd_uri_t uri;
};

};  // namespace http
