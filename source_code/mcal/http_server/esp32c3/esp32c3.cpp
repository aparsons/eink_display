#include "esp32c3.h"
#include "config.h"
#include "device_ctrl.h"

namespace http {

const char *TAG = "http";

static esp_err_t UriHandlerCallback(httpd_req_t *req);

HttpESP32C3::HttpESP32C3() {
}

HttpESP32C3::~HttpESP32C3() {
}

int32_t HttpESP32C3::Init(uri_t new_uri) {
  uri.uri      = new_uri.uri;
  uri.user_ctx = new_uri.user_ctx;
  uri.handler  = UriHandlerCallback;

  switch (new_uri.method) {
    case MethodType::GET:
      uri.method = HTTP_GET;
      break;
    case MethodType::POST:
      uri.method = HTTP_POST;
      break;
  }

  return 0;
}

int32_t HttpESP32C3::Uninit() {
  return 0;
}

handle_t HttpESP32C3::Start() {
  httpd_handle_t server    = NULL;
  const httpd_config_t cfg = HTTPD_DEFAULT_CONFIG();

  ESP_LOGI(TAG, "Starting server on port: %d", cfg.server_port);
  if (httpd_start(&server, &cfg) == ESP_OK) {
    ESP_LOGD(TAG, "Registering URI handlers");
    httpd_register_uri_handler(server, &uri);
    return server;
  }

  ESP_LOGE(TAG, "Error starting server");
  return NULL;
}

uint32_t HttpESP32C3::GetContentLength(req_t req) const {
  return static_cast<httpd_req_t *>(req)->content_len;
}

int32_t HttpESP32C3::ReceiveData(req_t req, char *buf, uint32_t buf_size) {
  esp_err_t rc = httpd_req_recv(static_cast<httpd_req_t *>(req), buf, buf_size);
  int32_t app_ret;

  if (rc < 0) {
    switch (rc) {
      case HTTPD_SOCK_ERR_TIMEOUT:
        app_ret = SOCKET_TIMEOUT;
        break;
      case HTTPD_SOCK_ERR_INVALID:
        app_ret = INVALID_ARGS;
        break;
      case HTTPD_SOCK_ERR_FAIL:
        app_ret = GENERIC_FAILURE;
        break;
      default:
        app_ret = UNKNOWN;
        break;
    }

    return app_ret;
  } else {
    return rc;
  }
}

int32_t HttpESP32C3::SendData(req_t req, const char *buf, uint32_t buf_size) {
  return httpd_resp_send_chunk(static_cast<httpd_req_t *>(req), buf, buf_size);
}

esp_err_t UriHandlerCallback(httpd_req_t *r) {
  // I really don't like that we've hardcoded the type of another module here but attempting to figure out templating
  // and interfacing with non-"void pointer" callbacks in ESP-IDF's C struct seemed like too much of a headache

  // TODO: figure out a more elegant solution than hardcoding DeviceController

  return static_cast<dev::DeviceController *>(r->user_ctx)->UriHandler(r);
}

};  // namespace http
