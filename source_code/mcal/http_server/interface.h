#pragma once

#include <stdint.h>
#include "etl/delegate.h"

namespace http {

typedef void *handle_t;
typedef void *req_t;

enum class MethodType {
  GET,
  POST,
};

typedef etl::delegate<int32_t(req_t *r)> handler_t;

typedef struct {
  const char *uri;
  MethodType method;
  void *user_ctx;
} uri_t;

// need to be defined as manual return codes because in ESP-IDF ReceiveData returns negative values for errors and
// positive values to signify the bytes remaining to read
constexpr int32_t GENERIC_FAILURE = -1;
constexpr int32_t SOCKET_TIMEOUT  = -2;
constexpr int32_t INVALID_ARGS    = -3;
constexpr int32_t UNKNOWN         = -4;

class IHttp {
 public:
  virtual ~IHttp() {
  }

  virtual int32_t Init(uri_t uri)                                         = 0;
  virtual int32_t Uninit()                                                = 0;
  virtual handle_t Start()                                                = 0;
  virtual uint32_t GetContentLength(req_t req) const                      = 0;
  virtual int32_t ReceiveData(req_t req, char *buf, uint32_t buf_size)    = 0;
  virtual int32_t SendData(req_t req, const char *buf, uint32_t buf_size) = 0;
};

};  // namespace http
