#pragma once

#include <stdint.h>

namespace spi {

class ISpi {
 public:
  virtual ~ISpi() {
  }

  virtual int32_t Init(bool use_cs)        = 0;
  virtual int32_t Uninit()                 = 0;
  virtual int32_t Transfer(const uint8_t *const tx_buffer,
                           uint32_t tx_len,
                           uint8_t *const rx_buffer,
                           uint32_t rx_len,
                           uint32_t flags) = 0;
};

};  // namespace spi
