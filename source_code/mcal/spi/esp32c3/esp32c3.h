#pragma once

#include "../interface.h"
#include "driver/spi_master.h"
#include <array>

namespace spi {

class SpiESP32C3 : public ISpi {
 public:
  SpiESP32C3(spi_host_device_t spi_host, uint8_t pin_mosi, uint8_t pin_miso, uint8_t pin_sck, uint8_t pin_cs);
  ~SpiESP32C3();

  int32_t Init(bool use_cs);
  int32_t Uninit();
  int32_t Transfer(const uint8_t *const tx_buffer,
                   uint32_t tx_len,
                   uint8_t *const rx_buffer,
                   uint32_t rx_len,
                   uint32_t flags);

 private:
  spi_device_handle_t spi;
  spi_host_device_t bus;
  uint8_t pin_mosi;
  uint8_t pin_miso;
  uint8_t pin_sck;
  uint8_t pin_cs;

  uint32_t clock_speed_hz;
};

};  // namespace spi
