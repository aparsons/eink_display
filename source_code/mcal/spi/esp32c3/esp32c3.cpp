#include "esp32c3.h"
#include "driver/gpio.h"
#include "esp_log.h"

namespace spi {

constexpr uint8_t MAX_SPI_XFER_SIZE = 64;
constexpr uint32_t BUS_SPEED        = 3 * 1000 * 1000;

SpiESP32C3::SpiESP32C3(spi_host_device_t spi_host, uint8_t pin_mosi, uint8_t pin_miso, uint8_t pin_sck, uint8_t pin_cs)
    : bus(spi_host), pin_mosi(pin_mosi), pin_miso(pin_miso), pin_sck(pin_sck), pin_cs(pin_cs) {
}

SpiESP32C3::~SpiESP32C3() {
}

int32_t SpiESP32C3::Init(bool use_cs) {
  esp_err_t rc = 0;

  const spi_bus_config_t bus_cfg = {
    .mosi_io_num     = static_cast<gpio_num_t>(pin_mosi),
    .miso_io_num     = -1,
    .sclk_io_num     = static_cast<gpio_num_t>(pin_sck),
    .quadwp_io_num   = -1,
    .quadhd_io_num   = -1,
    .max_transfer_sz = MAX_SPI_XFER_SIZE,
    .flags           = 0,
    .intr_flags      = 0,
  };

  spi_device_interface_config_t dev_cfg = {
    .command_bits     = 0,
    .address_bits     = 0,
    .dummy_bits       = 0,
    .mode             = 0,
    .duty_cycle_pos   = 128,
    .cs_ena_pretrans  = 0,
    .cs_ena_posttrans = 0,
    .clock_speed_hz   = BUS_SPEED,
    .input_delay_ns   = 0,
    .spics_io_num     = -1,
    .flags            = 0,
    .queue_size       = 1,
    .pre_cb           = NULL,
    .post_cb          = NULL,
  };

  if (use_cs) { dev_cfg.spics_io_num = pin_cs; }

  rc = spi_bus_initialize(bus, &bus_cfg, SPI_DMA_DISABLED);
  if (rc != ESP_OK) { return rc; }

  rc = spi_bus_add_device(bus, &dev_cfg, &spi);
  if (rc != ESP_OK) { return rc; }

  gpio_set_direction(static_cast<gpio_num_t>(pin_cs), GPIO_MODE_OUTPUT);
  gpio_set_level(static_cast<gpio_num_t>(pin_cs), 1);

  return rc;
}

int32_t SpiESP32C3::Uninit() {
  // TODO
  return ESP_OK;
}

int32_t SpiESP32C3::Transfer(const uint8_t *const tx_buffer,
                             uint32_t tx_len,
                             uint8_t *const rx_buffer,
                             uint32_t rx_len,
                             uint32_t flags) {
  esp_err_t rc        = 0;
  uint32_t bytes_sent = 0;
  uint32_t xfer_size  = 0;
  spi_transaction_t t = {};

  // The goal of the first iteration of this project was to have an image stored in the ESP32C3's flash and then have
  // the user enter which image they wanted to display through a CLI. It was thought that the RAM would become a
  // limiting factor pretty quickly if 260KB needed to be allocated for each image so storing things in a const array in
  // flash and then having a pointer just point to it would eliminate that issue. Unfortunately, you can only use SPI
  // DMA transfers (the preferred method) with a buffer that's stored in DRAM and marked with DRAM_ATTR and there's no
  // way an image was going to fit in there. The simplest, albeit inefficient, compromise was to just use polling
  // transfers because you can send anything on the stack but only in 64 byte chunks.

  // TODO: as the http web server receives image data in small packets and doesn't require full allocation of the image
  // buffer at once, the CLI could be modified as well to receive the image serially in the same fashion and allow for a
  // ~1KB DRAM array to be used and allow DMA SPI transactions project-wide

  while (bytes_sent < tx_len) {
    if ((tx_len - bytes_sent) >= MAX_SPI_XFER_SIZE) {
      xfer_size = MAX_SPI_XFER_SIZE;
    } else {
      xfer_size = (tx_len - bytes_sent);
    }

    t.length    = xfer_size * 8;
    t.rxlength  = t.length;
    t.tx_buffer = &tx_buffer[bytes_sent];

    rc = spi_device_polling_transmit(spi, &t);
    if (rc != ESP_OK) { return rc; }

    bytes_sent += xfer_size;
  }

  return rc;
}

};  // namespace spi
