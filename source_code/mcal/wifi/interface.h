#pragma once

#include <stdint.h>

namespace wifi {

enum class Mode {
  STATION,
};

class IWifi {
 public:
  virtual ~IWifi() {
  }

  virtual int32_t Init(Mode mode) = 0;
  virtual int32_t Uninit()        = 0;
  virtual int32_t Start()         = 0;
};

};  // namespace wifi
