#pragma once

#include "../interface.h"
#include "esp_wifi.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"

namespace wifi {

class WifiESP32C3 : public IWifi {
 public:
  WifiESP32C3(EventGroupHandle_t event_group);
  ~WifiESP32C3();

  int32_t Init(Mode mode);
  int32_t Uninit();
  int32_t Start();

  void EventHandler(const esp_event_base_t evt_base, const int32_t evt_id, const void *evt_data);

 private:
  uint8_t num_reconn_retries = 0;
  EventGroupHandle_t evt_group;

  esp_event_handler_instance_t inst_any_id;
  esp_event_handler_instance_t inst_got_ip;
};

};  // namespace wifi
