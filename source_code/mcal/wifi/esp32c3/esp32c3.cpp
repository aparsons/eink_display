#include "esp32c3.h"
#include "config.h"
#include "nvs_flash.h"
#include "device_ctrl.h"

namespace wifi {

constexpr uint8_t MAX_NUM_RETRIES = 5;

#define SSID CONFIG_WIFI_SSID
#define PASS CONFIG_WIFI_PASSWORD

const char *TAG = "WIFI";

static void EventHandlerCallback(void *arg, esp_event_base_t evt_base, int32_t evt_id, void *evt_data);

WifiESP32C3::WifiESP32C3(EventGroupHandle_t event_group) : evt_group(event_group) {
}

WifiESP32C3::~WifiESP32C3() {
}

int32_t WifiESP32C3::Init(Mode mode) {
  int32_t rc = 0;

  rc = nvs_flash_init();
  if ((rc == ESP_ERR_NVS_NO_FREE_PAGES) || (rc == ESP_ERR_NVS_NEW_VERSION_FOUND)) {
    rc = nvs_flash_erase();
    if (rc != 0) { return rc; }

    rc = nvs_flash_init();
    if (rc != 0) { return rc; }
  }

  ESP_LOGI(TAG, "Flash initialized");

  rc = esp_netif_init();
  if (rc != 0) { return rc; }

  rc = esp_event_loop_create_default();
  if (rc != 0) { return rc; }

  esp_netif_create_default_wifi_sta();

  const wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();

  rc = esp_wifi_init(&cfg);
  if (rc != 0) { return rc; }

  rc = esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &EventHandlerCallback, this, &inst_any_id);
  if (rc != 0) { return rc; }

  rc = esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &EventHandlerCallback, this, &inst_got_ip);
  if (rc != 0) { return rc; }

  wifi_config_t wifi_cfg = {
    .sta = {{.ssid = SSID},
            {.password = PASS},
            .threshold =
              {
                .authmode = WIFI_AUTH_WPA2_PSK,
              }},
  };

  wifi_mode_t esp_mode;
  switch (mode) {
    case Mode::STATION:
      esp_mode = WIFI_MODE_STA;
      break;
  }

  rc = esp_wifi_set_mode(esp_mode);
  if (rc != 0) { return rc; }

  rc = esp_wifi_set_config(WIFI_IF_STA, &wifi_cfg);
  if (rc != 0) { return rc; }

  return rc;
}

int32_t WifiESP32C3::Uninit() {
  return 0;
}

int32_t WifiESP32C3::Start() {
  int32_t rc = 0;

  rc = esp_wifi_start();

  return rc;
}

void WifiESP32C3::EventHandler(const esp_event_base_t evt_base, const int32_t evt_id, const void *evt_data) {
  if ((evt_base == WIFI_EVENT) && (evt_id == WIFI_EVENT_STA_START)) {
    ESP_LOGI(TAG, "Attempting to connect");
    esp_wifi_connect();
  } else if ((evt_base == WIFI_EVENT) && (evt_id == WIFI_EVENT_STA_DISCONNECTED)) {
    ESP_LOGI(TAG, "Failed to connect to AP");

    if (num_reconn_retries < MAX_NUM_RETRIES) {
      ESP_LOGI(TAG, "Attempting to reconnect");
      esp_wifi_connect();
      num_reconn_retries++;
      xEventGroupSetBits(evt_group, dev::WIFI_DISCONNECTED);
    } else {
      ESP_LOGE(TAG, "Failed to connect to SSID: %s, PW: %s", SSID, PASS);
      xEventGroupSetBits(evt_group, dev::WIFI_FAILED_TO_CONNECT);
    }
  } else if ((evt_base == IP_EVENT) && (evt_id == IP_EVENT_STA_GOT_IP)) {
    const ip_event_got_ip_t *evt = static_cast<const ip_event_got_ip_t *>(evt_data);
    ESP_LOGI(TAG, "Got IP:" IPSTR, IP2STR(&evt->ip_info.ip));

    num_reconn_retries = 0;
    xEventGroupSetBits(evt_group, dev::WIFI_CONNECTED);
    ESP_LOGI(TAG, "Connected to SSID: %s, PW: %s", SSID, PASS);
  }
}

static void EventHandlerCallback(void *arg, esp_event_base_t evt_base, int32_t evt_id, void *evt_data) {
  static_cast<WifiESP32C3 *>(arg)->EventHandler(evt_base, evt_id, evt_data);
}

};  // namespace wifi
