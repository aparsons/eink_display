# Testing with GoogleTest and GoogleMock

## Installation

1. Clone the latest GoogleTest release which at the time of writing this is 1.10.0 and then generate the Makefile with CMake

```bash
git clone https://github.com/google/googletest.git -b release-1.10.0
cd googletest        # Main directory of the cloned repository.
mkdir build          # Create a directory to hold the build output.
cd build
cmake ..             # Generate native build scripts for GoogleTest.
```

2. Build and install the GoogleTest and GoogleMock libraries

```bash
make -j8
sudo make install    # Install in /usr/local/ by default
```

## Building and Running Tests

1. Navigate to the build directory and then create another build directory if one doesn't exist

```bash
cd source_code/tests/build
mkdir build && cd build
```	

2. Generate the Makefile using CMake and then build the test project

```bash
cmake ..
make -j8
```

3. Run the test executable

```bash
./apg_test.out
```
