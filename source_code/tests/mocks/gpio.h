#pragma once

#include <gmock/gmock.h>
#include <string>

#include "mcal/gpio/interface.h"

class MockGpio : public gpio::IGpio {
 public:
  MOCK_METHOD(uint8_t, Read, (uint8_t pin), (const));
  MOCK_METHOD(int32_t, Write, (uint8_t pin, bool value), (const));
  MOCK_METHOD(int32_t, SetPull, (uint8_t pin, gpio::PULL pull_type), (const));
  MOCK_METHOD(int32_t, SetDirection, (uint8_t pin, gpio::DIRECTION direction), (const));
};
