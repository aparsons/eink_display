#pragma once

#include <gmock/gmock.h>
#include <string>

#include "mcal/uart/interface.h"

class MockUart : public uart::IUart {
 public:
  MOCK_METHOD(int32_t, Init, (uart::BaudRate baud, uart::Parity parity, uart::FlowControl flow_ctrl, uart::StopBits stop, uart::CharSize size), (override));
  MOCK_METHOD(int32_t, Uninit, (), (override));
  MOCK_METHOD(int32_t, ReadBytes, (char *const buffer), (override));
  MOCK_METHOD(int32_t, WriteBytes, (const char *const buffer, uint8_t num_bytes), (override));
  MOCK_METHOD(uint32_t, GetBufferSize, (), (const, override));
};
