#pragma once

#include <gmock/gmock.h>
#include <string>

#include "mcal/http_server/interface.h"

class MockHttp : public http::IHttp {
 public:
  MOCK_METHOD(int32_t, Init, (http::uri_t uri), (override));
  MOCK_METHOD(int32_t, Uninit, (), (override));
  MOCK_METHOD(http::handle_t, Start, (), (override));
  MOCK_METHOD(uint32_t, GetContentLength, (http::req_t req), (const, override));
  MOCK_METHOD(int32_t, ReceiveData, (http::req_t req, char *buf, uint32_t buf_size), (override));
  MOCK_METHOD(int32_t, SendData, (http::req_t req, const char *buf, uint32_t buf_size), (override));
};
