#pragma once

#include <gmock/gmock.h>
#include <string>

#include "mcal/spi/interface.h"

class MockSpi : public spi::ISpi {
 public:
  MOCK_METHOD(int32_t, Init, (bool use_cs), (override));
  MOCK_METHOD(int32_t, Uninit, (), (override));
  MOCK_METHOD(int32_t,
              Transfer,
              (const uint8_t *const tx_buffer,
               uint32_t tx_len,
               uint8_t *const rx_buffer,
               uint32_t rx_len,
               uint32_t flags),
              (override));
};
