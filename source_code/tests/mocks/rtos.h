#pragma once

#include <gmock/gmock.h>
#include <string>

#include "mcal/rtos/interface.h"

class MockRtos : public rtos::IRtos {
 public:
  MOCK_METHOD(rtos::task_t,
              TaskCreate,
              (const char *name, void (*function)(void *), uint32_t stack_size_bytes, void *params, uint32_t priority),
              (const));
  MOCK_METHOD(void, TaskDelete, (rtos::task_t task), (const));
  MOCK_METHOD(void, TaskDelayMs, (uint32_t ms_to_delay), (const));
  MOCK_METHOD(rtos::queue_t, QueueCreate, (uint32_t num_items, uint32_t item_size), (const));
  MOCK_METHOD(int32_t, QueueReceive, (rtos::queue_t queue, void *p_buffer, uint32_t ms_to_wait), (const));
  MOCK_METHOD(int32_t, QueueSend, (rtos::queue_t queue, const void *p_buffer, uint32_t ms_to_wait), (const));
  MOCK_METHOD(int32_t, SemaphoreTake, (rtos::semaphore_t sem, uint32_t ms_to_wait), (const));
  MOCK_METHOD(int32_t, SemaphoreGive, (rtos::semaphore_t sem), (const));
  MOCK_METHOD(rtos::evt_group_t, EventGroupCreate, (), (const));
  MOCK_METHOD(rtos::evt_bits_t,
              EventGroupWaitBits,
              (const rtos::evt_group_t group,
               const rtos::evt_bits_t bits_to_wait_for,
               const int32_t clear_on_exit,
               const int32_t wait_for_all_bits,
               uint32_t ms_to_wait),
              (const));
  MOCK_METHOD(rtos::evt_bits_t,
              EventGroupSetBits,
              (const rtos::evt_group_t group, const rtos::evt_bits_t bits_to_set),
              (const));
  MOCK_METHOD(rtos::evt_bits_t,
              EventGroupClearBits,
              (const rtos::evt_group_t group, const rtos::evt_bits_t bits_to_clear),
              (const));
};
