#pragma once

#include <gmock/gmock.h>
#include <string>

#include "mcal/wifi/interface.h"

class MockWifi : public wifi::IWifi {
 public:
  MOCK_METHOD(int32_t, Init, (wifi::Mode mode), (override));
  MOCK_METHOD(int32_t, Uninit, (), (override));
  MOCK_METHOD(int32_t, Start, (), (override));
};
