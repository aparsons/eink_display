#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ostream>

#include "mocks/gpio.h"
#include "mocks/spi.h"
#include "mocks/rtos.h"
#include "mocks/wifi.h"
#include "mocks/uart.h"
#include "mocks/http_server.h"
#include "app/cmd_handler.h"
#include "app/device_ctrl.h"
#include "app/uc8159.h"

using testing::_;
using testing::Assign;
using testing::DoAll;
using testing::Invoke;
using testing::InvokeWithoutArgs;
using testing::Return;
using testing::SaveArgPointee;
using testing::Sequence;
using testing::SetArgPointee;

// action that converts the second argument (0-based index, so arg1) into a Controller message
// packet
// ACTION_P(SetControllerMessagePacketToConstVoidArg1, value) {
//   *value = *reinterpret_cast<const scale_ctrl::MessagePacket*>(arg1);
// }

class DeviceControllerTests : public testing::Test {
 public:
  MockGpio *mock_gpio;
  MockSpi *mock_spi;
  MockRtos *mock_rtos;
  MockWifi *mock_wifi;
  MockUart *mock_uart;
  MockHttp *mock_http;

  cmd::CommandHandler *cmd_handler;
  uc8159::Uc8159 *uc8159_dev;
  dev::DeviceController *device_ctrl;

 protected:
  void SetUp() {
    mock_gpio = new MockGpio;
    mock_spi  = new MockSpi;
    mock_rtos = new MockRtos;
    mock_wifi = new MockWifi;
    mock_uart = new MockUart;
    mock_http = new MockHttp;

    uc8159_dev  = new uc8159::Uc8159(mock_spi, mock_gpio, mock_rtos, 0, 0, 0, 0);
    cmd_handler = new cmd::CommandHandler(mock_uart, mock_rtos, NULL);
    device_ctrl = new dev::DeviceController(NULL, mock_rtos, mock_wifi, mock_http, uc8159_dev, cmd_handler);
  }

  void TearDown() {
    delete device_ctrl;
    delete cmd_handler;
    delete uc8159_dev;
    delete mock_gpio;
    delete mock_spi;
    delete mock_rtos;
    delete mock_uart;
    delete mock_wifi;
    delete mock_http;
  }
};

/**
 * @brief Verify Device Controller starts HTTP server once WIFI connection occurs
 *
 * SETUP: Module has just been started and isn't performing any action
 * EVENT: Module receives EventGroup bit "WIFI_CONNECTED"
 * RESULT: Module starts HTTP Server
 */
TEST_F(DeviceControllerTests, HttpServerStartedAfterReceivingWifiConnectedEvent) {
  // SETUP *************************************************************************************************************

  // EXPECT ************************************************************************************************************
  EXPECT_CALL(*mock_rtos, EventGroupWaitBits(_, _, true, false, _)).WillOnce(Return(dev::WIFI_CONNECTED));
  EXPECT_CALL(*mock_http, Start()).Times(1);

  // RUN ***************************************************************************************************************
  device_ctrl->MailboxTask();
}
