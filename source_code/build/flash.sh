#!/bin/sh

if [ $# -eq 0 ]
then
  idf.py flash -p /dev/tty.usbserial-0001
  idf_monitor.py --port /dev/tty.usbserial-0001 --baud 115200 build/esp32c3.elf
else
  idf.py flash -p $1
  idf_monitor.py --port $1 --baud 115200 build/esp32c3.elf
fi
