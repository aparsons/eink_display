if [ $# -eq 0 ]
then
  idf_monitor.py --port /dev/tty.usbserial-0001 --baud 115200 build/esp32c3.elf
else
  idf_monitor.py --port $1 --baud 115200 build/esp32c3.elf
fi
