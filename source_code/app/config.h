#include <stdint.h>

#pragma once

constexpr int PIN_CS   = 9;
constexpr int PIN_SCK  = 5;
constexpr int PIN_MISO = 6;
constexpr int PIN_MOSI = 7;

constexpr int PIN_RST  = 10;
constexpr int PIN_BUSY = 1;
constexpr int PIN_DC   = 0;

constexpr uint16_t WIDTH  = 600;
constexpr uint16_t HEIGHT = 448;

constexpr uint32_t RESOLUTION = WIDTH * HEIGHT;

constexpr uint32_t MAX_DELAY = 0xFFFFFFFFUL;

#if defined(DISABLE_LOGGING)
// Replaces the ESP-provided log macros with dummy versions. Primarily used when we're doing unit
// testing and can't include the esp header files
#define ESP_LOGE(x, y, ...) \
  do {                      \
  } while (0)
#define ESP_LOGI(x, y, ...) \
  do {                      \
  } while (0)
#define ESP_LOGD(x, y, ...) \
  do {                      \
  } while (0)
#define ESP_LOGW(x, y, ...) \
  do {                      \
  } while (0)
#define ESP_LOGV(x, y, ...) \
  do {                      \
  } while (0)
#else
#include "log/include/esp_log.h"
#endif

#if defined(TESTING)
// used to exit infinite task loops by passing 0 as an argument. without this all tasks are untestable (infinite loops)
#define FOREVER 0
#define DELAY_US(x) \
  do {              \
  } while (0)
#else
#define FOREVER 1

#include "esp32c3/rom/ets_sys.h"
#define DELAY_US(x) ets_delay_us(x)

#endif
