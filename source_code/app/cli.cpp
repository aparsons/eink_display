#include <string.h>
#include "cli.h"

namespace cli {

bool IsAlphaNumericChar(char input);
int32_t HelpCommandCallback(Args args);

Cli::Cli() : help_cmd_cb(etl::delegate<int32_t(Args args)>::create<Cli, &Cli::HelpCommand>(*this)) {
}

Cli::~Cli() {
}

int32_t Cli::Init() {
  // assigns help command to head of command linked list
  RegisterHelpCommand();

  return 0;
}

int32_t Cli::Uninit() {
  // TODO
  return 0;
}

int32_t Cli::RegisterCommand(const cmd_t *const cmd) {
  static cmd_item_list_t *p_last_cmd_in_list = &registered_cmds;
  cmd_item_list_t *p_new_list_item;
  int32_t rc = -1;

  if (cmd == NULL) { return rc; }

  // allocates a new cmd item and appends it to the cmd linked list

  p_new_list_item = static_cast<cmd_item_list_t *>(malloc(sizeof(cmd_item_list_t)));

  if (p_new_list_item != NULL) {
    // add the new command item
    p_new_list_item->cmd    = cmd;
    p_new_list_item->p_next = NULL;

    // point last item to this new item
    p_last_cmd_in_list->p_next = p_new_list_item;

    // set end of list to new item
    p_last_cmd_in_list = p_new_list_item;

    rc = 0;
  }

  return rc;
}

int32_t Cli::ProcessCommand(const char *const input) {
  static const cmd_item_list_t *p_cmd = NULL;
  int32_t rc                          = 0;
  const char *p_registered_cmd;
  uint32_t cmd_len;
  Args args        = {};

  // search for command in list of registered commands
  for (p_cmd = &registered_cmds; p_cmd != NULL; p_cmd = p_cmd->p_next) {
    p_registered_cmd = p_cmd->cmd->cmd;
    cmd_len          = strlen(p_registered_cmd);

    // to make sure the commands match exactly and this isn't a sub-string of a longer command, check that the byte
    // after the expected end is either '\0' or space before parameter
    if (strncmp(input, p_registered_cmd, cmd_len) == 0) {
      if (input[cmd_len] == ' ') {
        // the command has been found and we have potential params
        (void)ExtractParams(&input[cmd_len + 1], args);
        break;
      } else if (input[cmd_len] == '\0') {
        // the command has been found and there are no params
        break;
      }

      // command was found but it's a sub-string so not a direct match
    }
  }

  if (p_cmd != NULL) {
    // the command was found so let's call the associated function
    rc = p_cmd->cmd->func(args);

    // TODO: handle the func ret codes?
  } else {
    // command not found
    rc = -1;
  }

  return rc;
}

void Cli::RegisterHelpCommand() {
  help_cmd.cmd  = "help";
  help_cmd.help = "\r\nhelp:\r\n Lists usage for registered commands\r\n\r\n";
  help_cmd.func = help_cmd_cb;

  // add the new command item
  registered_cmds.cmd    = &help_cmd;
  registered_cmds.p_next = NULL;
}

uint8_t Cli::ExtractParams(const char *input_after_cmd, Args &args) const {
  uint8_t curr_index        = 0;
  uint8_t num_args          = 0;
  int16_t param_start_index = -1;

  while ((input_after_cmd[curr_index] != '\0') && (input_after_cmd[curr_index] != '\n') &&
         (input_after_cmd[curr_index] != '\r')) {
    if (input_after_cmd[curr_index] == ' ') {
      // check if we're currently in the middle of a parameter string and only add the argument if we're under the limit
      // for max arguments
      if ((param_start_index != -1) && (num_args != MAX_NUM_ARGS)) {
        args[num_args].str = &input_after_cmd[param_start_index];
        args[num_args].len = (curr_index - param_start_index);
        num_args++;

        param_start_index = -1;
      }
    } else if (param_start_index == -1) {
      // store this index as the start of the parameter string
      param_start_index = curr_index;
    }

    curr_index++;
  }

  // handles the case where one of the terminating characters was the cause of the above loop exiting and the current
  // parameter wasn't fully stored
  if ((param_start_index != -1) && (num_args != MAX_NUM_ARGS)) {
    args[num_args].str = &input_after_cmd[param_start_index];
    args[num_args].len = (curr_index - param_start_index);
    num_args++;
  }

  return num_args;
}

int32_t Cli::HelpCommand(const Args args) const {
  int32_t rc = 0;
  if (args[0].len >= 1) {
    // we don't take any args for this command
    rc = -1;
  } else {
    const cmd_item_list_t *p_cmd = NULL;
    const char *p_registered_cmd;

    for (p_cmd = &registered_cmds; p_cmd != NULL; p_cmd = p_cmd->p_next) {
      // print the help text associated with all registered commands
      printf("%s", p_cmd->cmd->help);
    }
  }

  return rc;
}

bool IsAlphaNumericChar(char input) {
  if (((input >= '0') && (input <= '9')) || ((input >= 'A') && (input <= 'Z')) || ((input >= 'a') && (input <= 'z'))) {
    return true;
  } else {
    return false;
  }
}

};  // namespace cli
