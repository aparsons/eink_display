#pragma once

#include <array>
#include "mcal/uart/interface.h"
#include "etl/delegate.h"
#include "mcal/rtos/interface.h"
#include "cli.h"

namespace cmd {

class CommandHandler : public cli::Cli {
 public:
  CommandHandler(uart::IUart *const uart_interface, rtos::IRtos *const rtos_interface, rtos::semaphore_t uart_bin_sem);
  ~CommandHandler();

  int32_t Init();
  int32_t Uninit();

  void ProcessCommandTask();

 private:
  uart::IUart *const p_uart;
  rtos::IRtos *const p_rtos;
  rtos::semaphore_t uart_bin_sem;

  rtos::task_t process_cmd_task;
};

};  // namespace cmd
