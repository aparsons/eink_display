#include "uc8159.h"
#include "config.h"

namespace uc8159 {

static const char *TAG = "UC8159";

constexpr uint32_t NO_SPI_FLAGS = 0;

enum class Register : uint8_t {
  UC8159_PSR   = 0x00,
  UC8159_PWR   = 0x01,
  UC8159_POF   = 0x02,
  UC8159_PFS   = 0x03,
  UC8159_PON   = 0x04,
  UC8159_BTST  = 0x06,
  UC8159_DSLP  = 0x07,
  UC8159_DTM1  = 0x10,
  UC8159_DSP   = 0x11,
  UC8159_DRF   = 0x12,
  UC8159_IPC   = 0x13,
  UC8159_PLL   = 0x30,
  UC8159_TSC   = 0x40,
  UC8159_TSE   = 0x41,
  UC8159_TSW   = 0x42,
  UC8159_TSR   = 0x43,
  UC8159_CDI   = 0x50,
  UC8159_LPD   = 0x51,
  UC8159_TCON  = 0x60,
  UC8159_TRES  = 0x61,
  UC8159_DAM   = 0x65,
  UC8159_REV   = 0x70,
  UC8159_FLG   = 0x71,
  UC8159_AMV   = 0x80,
  UC8159_VV    = 0x81,
  UC8159_VDCS  = 0x82,
  UC8159_PWS   = 0xE3,
  UC8159_TSSET = 0xE5,
};

Uc8159::Uc8159(spi::ISpi *spi_interface,
               gpio::IGpio *gpio_interface,
               rtos::IRtos *rtos_interface,
               uint8_t dc_pin,
               uint8_t rst_pin,
               uint8_t busy_pin,
               uint8_t cs_pin)
    : p_spi(spi_interface),
      p_gpio(gpio_interface),
      p_rtos(rtos_interface),
      dc_pin(dc_pin),
      rst_pin(rst_pin),
      busy_pin(busy_pin),
      cs_pin(cs_pin) {
}

Uc8159::~Uc8159() {
}

int32_t Uc8159::Init() {
  int32_t rc = 0;

  rc = p_gpio->SetDirection(cs_pin, gpio::DIRECTION::OUTPUT);
  if (rc != 0) { return rc; }
  p_gpio->Write(cs_pin, 1);

  rc = p_gpio->SetDirection(dc_pin, gpio::DIRECTION::OUTPUT);
  if (rc != 0) { return rc; }
  p_gpio->Write(dc_pin, 0);

  rc = p_gpio->SetDirection(rst_pin, gpio::DIRECTION::OUTPUT);
  if (rc != 0) { return rc; }
  p_gpio->Write(rst_pin, 1);

  rc = p_gpio->SetDirection(busy_pin, gpio::DIRECTION::INPUT);
  if (rc != 0) { return rc; }
  rc = p_gpio->SetPull(busy_pin, gpio::PULL::DISABLED);
  if (rc != 0) { return rc; }

  rc = p_spi->Init(false);
  if (rc != 0) { return rc; }

  ESP_LOGI(TAG, "Initialized SPI module");

  p_gpio->Write(rst_pin, 0);
  p_rtos->TaskDelayMs(100);
  p_gpio->Write(rst_pin, 1);

  // required delay after reset
  p_rtos->TaskDelayMs(1000);

  buffer[0] = static_cast<uint8_t>(Register::UC8159_TRES);
  buffer[1] = 0b00000010;
  buffer[2] = 0b01011000;
  buffer[3] = 0b00000001;
  buffer[4] = 0b11000000;
  rc        = SendCommand(&buffer[0], 5);
  if (rc != 0) { return rc; }

  buffer[0] = static_cast<uint8_t>(Register::UC8159_PSR);
  buffer[1] = 0b11101111;
  buffer[2] = 0b00001000;
  rc        = SendCommand(&buffer[0], 3);
  if (rc != 0) { return rc; }

  buffer[0] = static_cast<uint8_t>(Register::UC8159_PWR);
  buffer[1] = 0b00110111;
  buffer[2] = 0b00000000;
  buffer[3] = 0b00100011;
  buffer[4] = 0b00100011;
  rc        = SendCommand(&buffer[0], 5);
  if (rc != 0) { return rc; }

  buffer[0] = static_cast<uint8_t>(Register::UC8159_PLL);
  buffer[1] = 0b00111100;
  rc        = SendCommand(&buffer[0], 2);
  if (rc != 0) { return rc; }

  buffer[0] = static_cast<uint8_t>(Register::UC8159_TSE);
  buffer[1] = 0b00000000;
  rc        = SendCommand(&buffer[0], 2);
  if (rc != 0) { return rc; }

  buffer[0] = static_cast<uint8_t>(Register::UC8159_CDI);
  buffer[1] = 0b00110111;
  rc        = SendCommand(&buffer[0], 2);
  if (rc != 0) { return rc; }

  buffer[0] = static_cast<uint8_t>(Register::UC8159_TCON);
  buffer[1] = 0b00100010;
  rc        = SendCommand(&buffer[0], 2);
  if (rc != 0) { return rc; }

  buffer[0] = static_cast<uint8_t>(Register::UC8159_DAM);
  buffer[1] = 0b00000000;
  rc        = SendCommand(&buffer[0], 2);
  if (rc != 0) { return rc; }

  buffer[0] = static_cast<uint8_t>(Register::UC8159_PWS);
  buffer[1] = 0b10101010;
  rc        = SendCommand(&buffer[0], 2);
  if (rc != 0) { return rc; }

  buffer[0] = static_cast<uint8_t>(Register::UC8159_PFS);
  buffer[1] = 0b00000000;
  rc        = SendCommand(&buffer[0], 2);
  if (rc != 0) { return rc; }

  return rc;
}

int32_t Uc8159::Uninit() {
  int32_t rc = 0;

  rc = p_spi->Uninit();

  return rc;
}

int32_t Uc8159::ShowFullImage(const uint8_t *const image_buffer) {
  int32_t rc = 0;

  // send command for beginning frame buffer transmission and set appropriate signals
  rc = StartSendingImage();
  if (rc != 0) { return rc; }

  // send frame buffer data in multiple SPI transactions
  rc = SendFrameData(image_buffer, RESOLUTION);
  if (rc != 0) { return rc; }

  // conclude frame buffer transactions and start refreshing display.
  // this function blocks for 30+ seconds
  rc = FinishSendingImage();
  if (rc != 0) { return rc; }

  return rc;
}

int32_t Uc8159::StartSendingImage() {
  int32_t rc = 0;

  buffer[0] = static_cast<uint8_t>(Register::UC8159_DTM1);
  rc        = SendCommand(&buffer[0], 1);

  p_gpio->Write(cs_pin, 0);
  p_gpio->Write(dc_pin, 1);

  ESP_LOGI(TAG, "Sent image start condition");

  return rc;
}

int32_t Uc8159::SendImageChunk(const uint8_t *const image_buffer, uint32_t buff_len) {
  int32_t rc = 0;

  rc = SendFrameData(image_buffer, buff_len);

  return rc;
}

int32_t Uc8159::FinishSendingImage() {
  int32_t rc;

  p_gpio->Write(cs_pin, 1);

  ESP_LOGI(TAG, "Power on");
  // turn on display driver
  buffer[0] = static_cast<uint8_t>(Register::UC8159_PON);
  rc        = SendCommand(&buffer[0], 1);
  if (rc != 0) { return rc; }

  p_rtos->TaskDelayMs(200);

  ESP_LOGI(TAG, "Waiting for refresh");
  // refresh the display with whatever is currently in the display buffer
  buffer[0] = static_cast<uint8_t>(Register::UC8159_DRF);
  rc        = SendCommand(&buffer[0], 1);
  if (rc != 0) { return rc; }

  // the refresh will typically take ~30s and is when the series of flashes appears on the screen
  WaitUntilReady(60);

  ESP_LOGI(TAG, "Power off");
  // turn off display driver
  buffer[0] = static_cast<uint8_t>(Register::UC8159_POF);
  rc        = SendCommand(&buffer[0], 1);
  if (rc != 0) { return rc; }

  p_rtos->TaskDelayMs(200);

  return rc;
}

int32_t Uc8159::SendCommand(const uint8_t *const data, uint8_t len) {
  int32_t rc = 0;

  // according to datasheet, this exact sequence needs to be followed when sending commands. First sending the command
  // with DC = 0 and making sure to de-assert CS to complete the transaction and then setting DC = 1 and transferring
  // the remaining bytes

  p_gpio->Write(cs_pin, 0);
  // configure for command mode
  p_gpio->Write(dc_pin, 0);

  rc = p_spi->Transfer(&data[0], 1, NULL, 0, NO_SPI_FLAGS);

  p_gpio->Write(cs_pin, 1);

  if (len > 1) {
    p_gpio->Write(cs_pin, 0);
    // configure for data mode
    p_gpio->Write(dc_pin, 1);

    rc = p_spi->Transfer(&data[1], (len - 1), NULL, 0, NO_SPI_FLAGS);

    p_gpio->Write(cs_pin, 1);
  }

  return rc;
}

int32_t Uc8159::SendFullFrameData(const uint8_t *const data, uint32_t len) {
  int32_t rc = 0;

  p_gpio->Write(cs_pin, 0);
  // configure for data mode
  p_gpio->Write(dc_pin, 1);

  rc = p_spi->Transfer(data, len, NULL, 0, NO_SPI_FLAGS);

  p_gpio->Write(cs_pin, 1);

  return rc;
}

int32_t Uc8159::SendFrameData(const uint8_t *const data, uint32_t len) {
  int32_t rc = 0;

  rc = p_spi->Transfer(data, len, NULL, 0, NO_SPI_FLAGS);

  return rc;
}

void Uc8159::WaitUntilReady(uint32_t seconds_to_wait) const {
  uint32_t busy_cnt = 0;

  while (busy_cnt < seconds_to_wait) {
    p_rtos->TaskDelayMs(1000);
    if (p_gpio->Read(busy_pin) == 1) {
      break;
    } else {
      busy_cnt++;
    }
  }
}

};  // namespace uc8159
