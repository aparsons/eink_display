#pragma once

#include <stdint.h>
#include "mcal/spi/interface.h"
#include "mcal/gpio/interface.h"
#include "mcal/rtos/interface.h"
#include <array>

namespace uc8159 {

class Uc8159 {
 public:
  Uc8159(spi::ISpi *const spi_interface,
         gpio::IGpio *const gpio_interface,
         rtos::IRtos *const rtos_interface,
         uint8_t dc_pin,
         uint8_t rst_pin,
         uint8_t busy_pin,
         uint8_t cs_pin);
  ~Uc8159();

  int32_t Init();
  int32_t Uninit();
  int32_t ShowFullImage(const uint8_t *const image_buffer);
  int32_t StartSendingImage();
  int32_t SendImageChunk(const uint8_t *const image_buffer, uint32_t buffer_len);
  int32_t FinishSendingImage();

 private:
  static const uint8_t BUFFER_SIZE = 20;

  int32_t SendCommand(const uint8_t *const data, uint8_t len);
  int32_t SendFullFrameData(const uint8_t *const data, uint32_t len);
  int32_t SendFrameData(const uint8_t *const data, uint32_t len);
  void WaitUntilReady(uint32_t seconds_to_wait) const;

  spi::ISpi *const p_spi;
  gpio::IGpio *const p_gpio;
  rtos::IRtos *const p_rtos;

  uint8_t dc_pin;
  uint8_t rst_pin;
  uint8_t busy_pin;
  uint8_t cs_pin;
  std::array<uint8_t, BUFFER_SIZE> buffer = {};
};

};  // namespace uc8159
