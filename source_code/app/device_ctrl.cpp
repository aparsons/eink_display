#include "device_ctrl.h"
#include <string.h>
#include "config.h"
#include <stdlib.h>
#include "fruit_image.h"

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

namespace dev {

constexpr uint16_t MAILBOX_TASK_SIZE  = 2 * 1024;
constexpr uint16_t INITIAL_READ_LEN   = 995;
constexpr uint16_t TRAILING_BYTES_LEN = 5;
constexpr uint16_t BUFFER_SIZE        = INITIAL_READ_LEN + TRAILING_BYTES_LEN;

constexpr uint32_t ACTIVE_BITS =
  (WIFI_CONNECTED | WIFI_DISCONNECTED | WIFI_FAILED_TO_CONNECT | SET_IMAGE_1 | REFRESH_FRAME);

const char *TAG      = "DEV";
const char *URI_PATH = "/image";

uint32_t ConvertStringToByteArray(char *string, uint8_t *bytes);
static void MailboxFreeTask(void *params);

DeviceController::DeviceController(rtos::evt_group_t event_group,
                                   rtos::IRtos *const rtos_interface,
                                   wifi::IWifi *const wifi_interface,
                                   http::IHttp *const http_interface,
                                   uc8159::Uc8159 *const uc8159_dev,
                                   cmd::CommandHandler *const cmd_handler)
    : p_rtos(rtos_interface),
      p_wifi(wifi_interface),
      p_http(http_interface),
      p_uc8159(uc8159_dev),
      p_cmd(cmd_handler),
      evt_group(event_group),
      uri_handler_cb(
        etl::delegate<int32_t(http::req_t r)>::create<DeviceController, &DeviceController::UriHandler>(*this)),
      set_image_cmd_cb(
        etl::delegate<int32_t(const cli::Args args)>::create<DeviceController, &DeviceController::SetImageCommand>(
          *this)) {
}

DeviceController::~DeviceController() {
}

int32_t DeviceController::Init() {
  int32_t rc = 0;

  rc = p_cmd->Init();
  if (rc != 0) { return rc; }

  ESP_LOGI(TAG, "Command Handler initialized");

  select_image_cmd.cmd  = "image";
  select_image_cmd.help = "\r\nimage:\r\n Displays an image on screen\r\n\r\n";
  select_image_cmd.func = set_image_cmd_cb;

  rc = p_cmd->RegisterCommand(&select_image_cmd);
  if (rc != 0) { return rc; }

  rc = p_uc8159->Init();
  if (rc != 0) { return rc; }

  ESP_LOGI(TAG, "UC8159 initialized");

  rc = p_wifi->Init(wifi::Mode::STATION);
  if (rc != 0) { return rc; }

  ESP_LOGI(TAG, "Wifi initialized");

  http::uri_t uri = {
    .uri      = URI_PATH,
    .method   = http::MethodType::POST,
    .user_ctx = this,
  };

  rc = p_http->Init(uri);
  if (rc != 0) { return rc; }

  ESP_LOGI(TAG, "HTTP initialized");

  mb_task = p_rtos->TaskCreate("dev_mb", MailboxFreeTask, MAILBOX_TASK_SIZE, this, 2);
  if (mb_task == NULL) { return -1; }

  return rc;
}

int32_t DeviceController::Uninit() {
  int32_t rc = 0;

  return rc;
}

int32_t DeviceController::Start() {
  int32_t rc = 0;

  rc = p_wifi->Start();

  return rc;
}

void DeviceController::MailboxTask() {
  ESP_LOGI(TAG, "Starting DeviceController MailboxTask");
  rtos::evt_bits_t evt_bits;
  int32_t rc = 0;

  do {
    evt_bits = p_rtos->EventGroupWaitBits(evt_group, ACTIVE_BITS, true, false, MAX_DELAY);

    if ((evt_bits & WIFI_CONNECTED) == WIFI_CONNECTED) {
      // once we're connected on wifi, start the web server
      http_handle = p_http->Start();
      if (http_handle == NULL) { ESP_LOGE(TAG, "Failed to start server"); }
    } else if ((evt_bits & WIFI_DISCONNECTED) == WIFI_DISCONNECTED) {
      // TODO: handle this
    } else if ((evt_bits & WIFI_FAILED_TO_CONNECT) == WIFI_FAILED_TO_CONNECT) {
      // TODO: handle this
    } else if ((evt_bits & SET_IMAGE_1) == SET_IMAGE_1) {
      rc = p_uc8159->ShowFullImage(static_cast<const uint8_t *>(&fruit_image[0]));
      if (rc != 0) { ESP_LOGE(TAG, "Failed to show full image: %d", rc); }
    } else if ((evt_bits & REFRESH_FRAME) == REFRESH_FRAME) {
      // We've received the last http packet for the image and have sent it over SPI. We can now signal for the display
      // to refresh.
      rc = p_uc8159->FinishSendingImage();
      if (rc != 0) { ESP_LOGE(TAG, "Failed to finish image: %d", rc); }
    } else {
      // max delay elapsed without any bits being sent
    }
  } while (FOREVER);
}

int32_t DeviceController::UriHandler(http::req_t r) {
  // ESP_LOGI(TAG, "Got in URI handler");

  // The bytes array only needs to be half of the char buffer's size because all numbers are comma-separated. The
  // worst-case scenario would be all single digit numbers but that would still mean half of the characters are numbers
  // and we're fine.
  uint8_t bytes[BUFFER_SIZE / 2] = {};
  char buf[BUFFER_SIZE]          = {};
  int32_t ret                    = 0;
  uint32_t bytes_read            = 0;
  int32_t rc                     = 0;

  auto remaining = p_http->GetContentLength(r);
  ESP_LOGI(TAG, "Content Length: %d", remaining);

  // initiate start of image transfer
  p_uc8159->StartSendingImage();

  while (remaining > 0) {
    // NOTE: Even though the default RX buffer for wifi is 1600 bytes, the number of bytes actually read each time will
    // vary significantly. Don't assume you're reading the number of bytes you're requesting here. The return value
    // should be used as a guide.
    if ((ret = p_http->ReceiveData(r, buf, MIN(remaining, (sizeof(buf) - TRAILING_BYTES_LEN)))) <= 0) {
      if (ret == http::SOCKET_TIMEOUT) { continue; }

      ESP_LOGI(TAG, "Error: %d", ret);
      return -1;
    }

    bytes_read = ret;

    // The current implementation of the image's format is just a long string of bytes with each byte delimited by a
    // comma. Each byte is 8 bits so we're getting values from 0 to 255. As the number of characters we read during each
    // iteration of this main loop is different, we always need to consider that our read cut off in the middle of a
    // number.
    //
    // Example of last 3 characters of a read: '1' ',' '2'
    //
    // If our last character is '2', we have no idea if the full number of that byte is 2 or 25 or 204. So in the
    // following loop, we will read one byte out at a time and check if it's a comma. If it is, we know that our buffer
    // is "terminated" properly and can move on. As the largest 8-bit value is 3 digits, giving ourselves 5 bytes of
    // room should always be enough in the worst-case scenario.
    for (auto i = 0; i < TRAILING_BYTES_LEN; i++) {
      if (buf[bytes_read - 1] != ',') {
        if ((ret = p_http->ReceiveData(r, &buf[bytes_read], 1)) <= 0) {
          ESP_LOGE(TAG, "Error: %d", ret);
          return -1;
        }

        bytes_read++;
      } else {
        break;
      }
    }

    // terminate the string
    buf[bytes_read] = '\0';

    // parse char array for numbers and store them into uint8_t array
    auto bytes_converted = ConvertStringToByteArray(buf, bytes);

    // send bytes to display
    rc = p_uc8159->SendImageChunk(bytes, bytes_converted);
    if (rc != 0) { ESP_LOGE(TAG, "Failed to send chunk"); }

    remaining -= bytes_read;
  }

  // signal end of image transfer
  p_rtos->EventGroupSetBits(evt_group, REFRESH_FRAME);

  // end response
  p_http->SendData(r, NULL, 0);
  return 0;
}

int32_t DeviceController::SetImageCommand(const cli::Args args) const {
  int32_t rc = 0;

  if (args[0].len >= 1) {
    if (!strncmp("1", args[0].str, args[0].len)) {
      // Command: image 1
      ESP_LOGI(TAG, "Setting image 1");
      p_rtos->EventGroupSetBits(evt_group, SET_IMAGE_1);
    } else {
      return -1;
    }
  } else {
    // need arguments
    rc = -1;
  }

  return rc;
}

uint32_t ConvertStringToByteArray(char *string, uint8_t *bytes) {
  char *curr_val;
  uint32_t i = 0;

  // tokenizes the input based on commas and converts the tokens into integers
  curr_val = strtok(string, ",");
  while (curr_val != NULL) {
    bytes[i++] = atoi(curr_val);
    curr_val   = strtok(NULL, ",");
  }

  return i;
}

static void MailboxFreeTask(void *params) {
  static_cast<DeviceController *>(params)->MailboxTask();
}

};  // namespace dev
