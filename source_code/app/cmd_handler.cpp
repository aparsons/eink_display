#include "cmd_handler.h"
#include "config.h"
#include <string.h>

namespace cmd {

constexpr uint16_t PROCESS_CMD_TASK_SIZE = 3 * 1024;

const char *TAG = "CMD";

static void ProcessCommandFreeTask(void *params);

CommandHandler::CommandHandler(uart::IUart *const uart_interface,
                               rtos::IRtos *const rtos_interface,
                               rtos::semaphore_t uart_bin_sem)
    : p_uart(uart_interface), p_rtos(rtos_interface), uart_bin_sem(uart_bin_sem) {
}

CommandHandler::~CommandHandler() {
}

int32_t CommandHandler::Init() {
  int32_t rc = p_uart->Init(uart::BaudRate::RATE_115200,
                            uart::Parity::NONE,
                            uart::FlowControl::DISABLED,
                            uart::StopBits::BITS_1,
                            uart::CharSize::SIZE_8_BITS);
  if (rc != 0) { return rc; }

  rc = Cli::Init();
  if (rc != 0) { return rc; }

  ESP_LOGI(TAG, "Initialized CLI");

  process_cmd_task = p_rtos->TaskCreate("process_cmd", ProcessCommandFreeTask, PROCESS_CMD_TASK_SIZE, this, 2);
  if (process_cmd_task == NULL) { return -1; }

  return rc;
}

int32_t CommandHandler::Uninit() {
  // TODO
  return 0;
}

void CommandHandler::ProcessCommandTask() {
  ESP_LOGI(TAG, "Starting ProcessCommandTask");
  char *buff = static_cast<char *>(calloc(p_uart->GetBufferSize(), sizeof(char)));
  int32_t rc;

  do {
    if (p_rtos->SemaphoreTake(uart_bin_sem, MAX_DELAY)) {
      // received string with either '\n' or '\r' as a terminating character

      // read out the string stored in UART's buffer
      auto buff_len = p_uart->ReadBytes(buff);
      if (buff_len > 0) {
        // null terminate string so that command/params can match
        buff[buff_len - 1] = '\0';
        // ESP_LOGI(TAG, "Got command: %s, Length: %d", buff, len);
        rc = Cli::ProcessCommand(buff);
        if (rc != 0) { ESP_LOGE(TAG, "Error [%d]", rc); }
      } else {
        ESP_LOGE(TAG, "Failed to get buffer data");
      }
    }
  } while (FOREVER);
}

static void ProcessCommandFreeTask(void *params) {
  static_cast<CommandHandler *>(params)->ProcessCommandTask();
}

};  // namespace cmd
