#pragma once

#include "mcal/http_server/interface.h"
#include "mcal/wifi/interface.h"
#include "mcal/rtos/interface.h"
#include "uc8159.h"
#include "cmd_handler.h"

namespace dev {

constexpr uint32_t WIFI_CONNECTED         = (1 << 0);
constexpr uint32_t WIFI_DISCONNECTED      = (1 << 1);
constexpr uint32_t WIFI_FAILED_TO_CONNECT = (1 << 2);
constexpr uint32_t SET_IMAGE_1            = (1 << 3);
constexpr uint32_t REFRESH_FRAME          = (1 << 4);

class DeviceController {
 public:
  DeviceController(rtos::evt_group_t event_group,
                   rtos::IRtos *const rtos_interface,
                   wifi::IWifi *const wifi_interface,
                   http::IHttp *const http_interface,
                   uc8159::Uc8159 *const uc8159_dev,
                   cmd::CommandHandler *const cmd_handler);
  ~DeviceController();

  int32_t Init();
  int32_t Uninit();
  int32_t Start();

  void MailboxTask();
  int32_t UriHandler(http::req_t r);
  int32_t SetImageCommand(const cli::Args args) const;

 private:
  rtos::IRtos *const p_rtos;
  wifi::IWifi *const p_wifi;
  http::IHttp *const p_http;
  uc8159::Uc8159 *const p_uc8159;
  cmd::CommandHandler *const p_cmd;

  rtos::evt_group_t evt_group;
  rtos::task_t mb_task;
  http::handle_t http_handle;

  etl::delegate<int32_t(http::req_t r)> uri_handler_cb;
  etl::delegate<int32_t(cli::Args args)> set_image_cmd_cb;
  cli::cmd_t select_image_cmd;
};

uint32_t ConvertStringToByteArray(char *string, uint8_t *bytes);

};  // namespace dev
