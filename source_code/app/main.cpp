// Copyright 2021 <Some Dude>

#include <stdio.h>

#include "config.h"
#include "mcal/spi/esp32c3/esp32c3.h"
#include "mcal/gpio/esp32c3/esp32c3.h"
#include "mcal/rtos/esp32c3/esp32c3.h"
#include "mcal/uart/esp32c3/esp32c3.h"
#include "mcal/wifi/esp32c3/esp32c3.h"
#include "mcal/http_server/esp32c3/esp32c3.h"
#include "uc8159.h"
#include "cmd_handler.h"
#include "device_ctrl.h"

const char *TAG = "MAIN";

extern "C" void app_main(void) {
  int32_t rc                      = 0;
  SemaphoreHandle_t uart_bin_sem  = xSemaphoreCreateBinary();
  EventGroupHandle_t dev_ctrl_grp = xEventGroupCreate();

  // initialize interface components
  rtos::RtosESP32C3 rtos_dev;
  gpio::GpioESP32C3 gpio_dev;
  spi::SpiESP32C3 spi_dev(SPI2_HOST, PIN_MOSI, PIN_MISO, PIN_SCK, PIN_CS);
  uart::UartESP32C3 uart_dev(UART_NUM_0, uart_bin_sem);
  wifi::WifiESP32C3 wifi_dev(dev_ctrl_grp);
  http::HttpESP32C3 http_server;

  // initialize application-level components
  uc8159::Uc8159 uc8159_dev(&spi_dev, &gpio_dev, &rtos_dev, PIN_DC, PIN_RST, PIN_BUSY, PIN_CS);
  cmd::CommandHandler cmd_handler(&uart_dev, &rtos_dev, uart_bin_sem);
  dev::DeviceController device(dev_ctrl_grp, &rtos_dev, &wifi_dev, &http_server, &uc8159_dev, &cmd_handler);

  rc = device.Init();
  if (rc != 0) {
    ESP_LOGE(TAG, "Failed to initialize application");
  } else {
    ESP_LOGI(TAG, "Starting application");
    device.Start();
  }

  while (1) {
    rtos_dev.TaskDelayMs(1000);
  }
}
