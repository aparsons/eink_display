#pragma once

#include <array>
#include "etl/delegate.h"

namespace cli {

constexpr uint8_t MAX_NUM_ARGS = 10;

typedef struct {
  const char *str;
  uint8_t len;
} arg_t;

typedef std::array<arg_t, MAX_NUM_ARGS> Args;
typedef etl::delegate<int32_t(const Args args)> cli_cb_t;

typedef struct {
  const char *cmd;
  const char *help;
  cli_cb_t func;
} cmd_t;

class Cli {
 protected:
  int32_t Init();
  int32_t Uninit();

 public:
  Cli();
  ~Cli();

  int32_t RegisterCommand(const cmd_t *const cmd);
  int32_t ProcessCommand(const char *const input);

  int32_t HelpCommand(const Args args) const;

 private:
  typedef struct cmd_item_list {
    const cmd_t *cmd;
    struct cmd_item_list *p_next;
  } cmd_item_list_t;

  cmd_t help_cmd                  = {};
  cmd_item_list_t registered_cmds = {};

  etl::delegate<int32_t(const Args args)> help_cmd_cb;

  void RegisterHelpCommand();
  uint8_t ExtractParams(const char *input_after_cmd, Args &args) const;
};

};  // namespace cli
