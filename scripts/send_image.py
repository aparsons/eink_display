#!/usr/bin/env python3

import sys
import numpy
import requests

from PIL import Image

DESATURATED_PALETTE = [
    [0, 0, 0],
    [255, 255, 255],
    [0, 255, 0],
    [0, 0, 255],
    [255, 0, 0],
    [255, 255, 0],
    [255, 140, 0],
    [255, 255, 255]
]

SATURATED_PALETTE = [
    [57, 48, 57],
    [255, 255, 255],
    [58, 91, 70],
    [61, 59, 94],
    [156, 72, 75],
    [208, 190, 71],
    [177, 106, 73],
    [255, 255, 255]
]

IMAGE_WIDTH = 600
IMAGE_HEIGHT = 448
RESOLUTION = IMAGE_WIDTH * IMAGE_HEIGHT

def palette_blend(saturation, dtype='uint8'):
  saturation = float(saturation)
  palette = []
  for i in range(7):
      rs, gs, bs = [c * saturation for c in SATURATED_PALETTE[i]]
      rd, gd, bd = [c * (1.0 - saturation) for c in DESATURATED_PALETTE[i]]
      if dtype == 'uint8':
          palette += [int(rs + rd), int(gs + gd), int(bs + bd)]
      if dtype == 'uint24':
          palette += [(int(rs + rd) << 16) | (int(gs + gd) << 8) | int(bs + bd)]
  if dtype == 'uint8':
      palette += [255, 255, 255]
  if dtype == 'uint24':
      palette += [0xffffff]
  return palette


def ensure_correct_image_size(image):
  if (image.size != (IMAGE_WIDTH, IMAGE_HEIGHT)):
    print("Wrong size: {}. Resizing to ({}, {})".format(image.size, IMAGE_WIDTH, IMAGE_HEIGHT))
    image = image.resize((IMAGE_WIDTH, IMAGE_HEIGHT))

  return image


def main():
  if (len(sys.argv) < 4):
    print("Usage <path to image> <saturation> <ip addr>")
    exit(0)

  image_path = sys.argv[1]

  try:
    saturation = float(sys.argv[2])
  except Exception as e:
    print(e)
    exit(1)

  ip_addr = sys.argv[3]

  print("Preparing image")

  image = Image.open(image_path)
  image = ensure_correct_image_size(image)

  if not image.mode == "P":
    palette = palette_blend(saturation)

    palette_image = Image.new("P", (1,1))

    palette_image.putpalette(palette + [0, 0, 0] * 248)

    image.load()
    image = image.im.convert("P", True, palette_image.im)


  buf = numpy.array(image, dtype=numpy.uint8).reshape((IMAGE_WIDTH, IMAGE_HEIGHT))
  buf = buf.flatten()

  buf = ((buf[::2] << 4) & 0xF0) | (buf[1::2] & 0x0F)

  buf = buf.astype('uint8').tolist()
  buf = str(buf)

  buf = buf[1:].replace(' ','')
  buf = buf[:-1] + ','

  print("Processed image")

  print("Sending image")

  r = requests.post('http://{}/image'.format(ip_addr), data=buf)
  print(r.text)


if __name__ == "__main__":
  main()
